<?php

namespace coresoft\settings\controllers;

/**
 * ReadingController implements Reading Settings page.
 *
 * @author José Peña <joepa37@gmail.com>
 */
class ReadingController extends SettingsBaseController
{
    public $modelClass = 'coresoft\settings\models\ReadingSettings';
    public $viewPath = '@coresoft/settings/views/reading/index';

}