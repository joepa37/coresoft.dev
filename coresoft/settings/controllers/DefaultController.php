<?php

namespace coresoft\settings\controllers;

/**
 * DefaultController implements General Settings page.
 *
 * @author José Peña <joepa37@gmail.com>
 */
class DefaultController extends SettingsBaseController
{
    public $modelClass = 'coresoft\settings\models\GeneralSettings';
    public $viewPath = '@coresoft/settings/views/default/index';

}