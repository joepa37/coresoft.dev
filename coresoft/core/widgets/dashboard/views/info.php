<?php
use coresoft\Core;

/* @var $this yii\web\View */
?>

<div class="pull-<?= $position ?> col-lg-<?= $width ?> widget-height-<?= $height ?>">
    <div class="panel panel-default">
        <div class="panel-heading"><?= Yii::t('core', 'System Info') ?></div>
        <div class="panel-body">
            <b><?= Yii::t('core', 'CoreSoft Version') ?>:</b> <?= Yii::$app->params['version']; ?><br/>
            <b><?= Yii::t('core', 'Core Version') ?>:</b> <?= Core::getVersion(); ?><br/>
            <b><?= Yii::t('core', 'Yii Framework Version') ?>:</b> <?= Yii::getVersion(); ?><br/>
            <b><?= Yii::t('core', 'PHP Version') ?>:</b> <?= phpversion(); ?><br/>
        </div>
    </div>
</div>