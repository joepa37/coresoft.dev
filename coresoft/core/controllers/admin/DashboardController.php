<?php

namespace coresoft\controllers\admin;

use yii\helpers\ArrayHelper;

class DashboardController extends BaseController
{
    /**
     * @inheritdoc
     */
    public $enableOnlyActions = ['index'];
    public $widgets = NULL;

    public function actions()
    {

        if ($this->widgets === NULL) {
            $this->widgets = [
                'coresoft\comment\widgets\dashboard\Comments',
                [
                    'class' => 'coresoft\widgets\dashboard\Info',
                    'position' => 'right',
                ],

                [
                    'class' => 'coresoft\media\widgets\dashboard\Media',
                    'position' => 'right',
                ],
                'coresoft\post\widgets\dashboard\Posts',
                'coresoft\user\widgets\dashboard\Users',
            ];
        }

        return ArrayHelper::merge(parent::actions(), [
            'index' => [
                'class' => 'coresoft\web\DashboardAction',
                'widgets' => $this->widgets,
            ]
        ]);
    }
}