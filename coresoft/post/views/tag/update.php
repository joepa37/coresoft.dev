<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model coresoft\post\models\Tag */

$this->title = Yii::t('core/media', 'Update Tag');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/post', 'Posts'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/post', 'Tags'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('core', 'Update');
?>
<div class="post-tag-update">
    <h3 class="lte-hide-title"><?= Html::encode($this->title) ?></h3>
    <?= $this->render('_form', compact('model')) ?>
</div>