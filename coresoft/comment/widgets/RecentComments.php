<?php

namespace coresoft\comment\widgets;

use coresoft\comment\models\search\CommentSearch;
use coresoft\comments\models\Comment;
use coresoft\models\User;
use coresoft\widgets\DashboardWidget;
use Yii;

class RecentComments extends DashboardWidget
{

    /**
     * Most recent comments limit
     */
    public $recentLimit = 5;
    
    public $layout = 'layout';
    public $commentTemplate = 'comment';

    public function run()
    {
        $recentComments = Comment::find()
                ->active()
                ->orderBy(['created_at' => SORT_DESC])
                ->limit($this->recentLimit)
                ->all();

        return $this->render($this->layout, [
            'recentComments' => $recentComments,
            'commentTemplate' => $this->commentTemplate,
        ]);
    }

}
