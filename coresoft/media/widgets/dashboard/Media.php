<?php

namespace coresoft\media\widgets\dashboard;

use coresoft\media\models\Media as MediaModel;
use coresoft\models\User;
use coresoft\widgets\DashboardWidget;

class Media extends DashboardWidget
{
    /**
     * Most recent post limit
     */
    public $recentLimit = 5;

    /**
     * Post index action
     */
    public $indexAction = 'media/default/index';

    public function run()
    {
        if (User::hasPermission('viewMedia')) {
            $recent = MediaModel::find()->orderBy(['id' => SORT_DESC])->limit($this->recentLimit)->all();

            return $this->render('media',
                [
                    'height' => $this->height,
                    'width' => $this->width,
                    'position' => $this->position,
                    'recent' => $recent,
                ]);
        }
    }
}