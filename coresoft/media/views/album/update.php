<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model coresoft\media\models\Album */

$this->title = Yii::t('core', 'Update {item}', ['item' => Yii::t('core/media', 'Album')]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/media', 'Media'), 'url' => ['/media/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/media', 'Albums'), 'url' => ['/media/album/index']];
$this->params['breadcrumbs'][] = Yii::t('core', 'Update');
?>
<div class="album-update">
    <h3 class="lte-hide-title"><?= Html::encode($this->title) ?></h3>
    <?= $this->render('_form', compact('model')) ?>
</div>