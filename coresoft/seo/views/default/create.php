<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model coresoft\seo\models\Seo */

$this->title = Yii::t('core/seo', 'Create SEO Record');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/seo', 'SEO'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seo-create">
    <h3 class="lte-hide-title"><?= Html::encode($this->title) ?></h3>
    <?= $this->render('_form', compact('model')) ?>
</div>
