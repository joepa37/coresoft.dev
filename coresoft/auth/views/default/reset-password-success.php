<?php

/**
 * @var yii\web\View $this
 */

$this->title = Yii::t('core/auth', 'Password recovery');
?>
<div class="password-recovery-success">

    <div class="alert alert-success text-center">
        <?= Yii::t('core/auth', 'Check your E-mail for further instructions') ?>
    </div>

</div>
