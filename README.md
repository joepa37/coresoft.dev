# CORE ADVANCED
CoreAdvanced - Control Panel Based On Yii2 PHP Framework

Installation
------------

### Installing CoreSoft application. 

  1. Installing (using Composer)

    If you do not have [Composer](http://getcomposer.org/), follow the instructions in the
    [Installing Yii](https://github.com/yiisoft/yii2/blob/master/docs/guide/start-installation.md#installing-via-composer) section of the definitive guide to install it.

    With Composer installed, you can then install the application using the following commands:

    ```bash
    cd /var/www/
    composer global require "fxp/composer-asset-plugin:^1.2.0"
    git clone git@gitlab.com:joepa37/coresoft.dev.git
    cd coresoft.dev
    composer install
    ```

  2. Initialize the installed application

     Execute the `init` command and select `dev` or `prod` as environment.

      ```bash
      cd /var/www/coresoft.dev/
      php init
      ```
  
  3. Configurate your web server:

     For Apache config file could be the following:
     
     ```apacheconf
     <VirtualHost *:80>
       ServerName coresoft.dev
       ServerAlias www.coresoft.dev
       DocumentRoot "/var/www/coresoft.dev/"
       <Directory "/var/www/coresoft.dev/">
         AllowOverride All
       </Directory>
     </VirtualHost>
     ```
     For Nginx config file could be the following:
     
     ```nginx
     server {
         charset      utf-8;
         client_max_body_size  200M;
         listen       80;
     
         server_name  mysite.com;
         root         /var/www/mysite.com;
     
         location / {
             root  /var/www/mysite.com/frontend/web;
             try_files  $uri /frontend/web/index.php?$args;
     
             # avoiding processing of calls to non-existing static files by Yii
             location ~ \.(js|css|png|jpg|gif|swf|ico|pdf|mov|fla|zip|rar)$ {
                 access_log  off;
                 expires  360d;
                 try_files  $uri =404;
             }
         }
     
         location /admin {
             alias  /var/www/mysite.com/backend/web;
             rewrite  ^(/admin)/$ $1 permanent;
             try_files  $uri /backend/web/index.php?$args;
         }
     
         # avoiding processing of calls to non-existing static files by Yii
         location ~ ^/admin/(.+\.(js|css|png|jpg|gif|swf|ico|pdf|mov|fla|zip|rar))$ {
             access_log  off;
             expires  360d;
     
             rewrite  ^/admin/(.+)$ /backend/web/$1 break;
             rewrite  ^/admin/(.+)/(.+)$ /backend/web/$1/$2 break;
             try_files  $uri =404;
         }
     
         location ~ \.php$ {
             include  fastcgi_params;
             # check your /etc/php5/fpm/pool.d/www.conf to see if PHP-FPM is listening on a socket or port
             fastcgi_pass  unix:/var/run/php5-fpm.sock; ## listen for socket
             #fastcgi_pass  127.0.0.1:9000; ## listen for port
             fastcgi_param  SCRIPT_FILENAME $document_root$fastcgi_script_name;
             try_files  $uri =404;
         }
         #error_page  404 /404.html;
     
         location = /requirements.php {
             deny all;
         }
     
         location ~ \.(ht|svn|git) {
             deny all;
         }
     }
     ```
     
       
  4. Create a new database and adjust the `components['db']` configuration in `common/config/main-local.php` accordingly.

  5. Apply all migrations with console command 
  ```
  php yii migrate --migrationLookup=@coresoft/core/migrations/,@coresoft/auth/migrations/,@coresoft/settings/migrations/,@coresoft/menu/migrations/,@coresoft/user/migrations/,@coresoft/translation/migrations/,@coresoft/media/migrations/,@coresoft/post/migrations/,@coresoft/page/migrations/,@coresoft/comments/migrations/,@coresoft/comment/migrations/,@coresoft/seo/migrations/
  ```

  6. Init root user with console command 
  ```
  php yii init-admin
  ```

  7. Configurate your mailer `['components']['mailer']` in `common/config/main-local.php`.

##### Your `CoreSoft` application is installed. Visit your site `coresoft.dev` or admin panel `coresoft.dev/admin`, the site should work and message _Congratulations! You have successfully created your Yii-powered application_ should be displayed.
