<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'bootstrap' => ['comments', 'core'],
    'language' => 'en-US',
    'sourceLanguage' => 'en-US',
    'components' => [
        'core' => [
            'class' => 'coresoft\Core',
        ],
        'settings' => [
            'class' => 'coresoft\components\Settings'
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'class' => 'coresoft\components\User',
            'on afterLogin' => function ($event) {
                \coresoft\models\UserVisitLog::newVisitor($event->identity->id);
            }
        ],
    ],
    'modules' => [
        'comments' => [
            'class' => 'coresoft\comments\Comments',
            'userModel' => 'coresoft\models\User',
            'userAvatar' => function ($user_id) {
                $user = coresoft\models\User::findIdentity((int)$user_id);
                if ($user instanceof coresoft\models\User) {
                    return $user->getAvatar();
                }
                return false;
            }
        ],
        'gii' => [
	        'class' => 'yii\gii\Module',
	        'allowedIPs' => ['*'],
	        'generators' => [
		        'core-crud' => [
			        'class' => 'coresoft\generator\crud\Generator',
			        'templates' => [
				        'default' => '@coresoft/generator/crud/core-admin',
			        ]
		        ],
	        ],
        ],
    ],
];
